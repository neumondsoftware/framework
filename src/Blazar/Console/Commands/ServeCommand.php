<?php

namespace Blazar\Console\Commands;

use Neumond\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ServeCommand
 * @package Blazar\Console\Commands
 */
class ServeCommand extends Command
{
    /**
     * Command name
     */
    public const COMMAND_NAME = 'serve';

    /**
     * Command description
     */
    public const COMMAND_DESCRIPTION = 'Start a HTTP server';

    /**
     * Command help
     */
    public const COMMAND_HELP = 'This command starts a HTTP server for local development';

    /**
     * RouteCommand constructor.
     */
    public function __construct()
    {
        parent::__construct(self::COMMAND_NAME);
    }

    /**
     * Configure the command
     */
    protected function configure(): void
    {
        $this->setDescription(self::COMMAND_DESCRIPTION);
        $this->setHelp(self::COMMAND_HELP);
    }

    /**
     * Execute the command
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Starting server on http://localhost:8000');
        passthru('php -S localhost:8000 -t public');
        return Command::SUCCESS;
    }
}
