<?php

namespace Blazar\Console\Commands;

use Neumond\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RouteCommand
 * @package Blazar\Console\Commands
 */
class RouteCommand extends Command
{
    /**
     * Command name
     */
    public const COMMAND_NAME = 'routes';

    /**
     * Command description
     */
    public const COMMAND_DESCRIPTION = 'List all routes';

    /**
     * Command help
     */
    public const COMMAND_HELP = 'This command lists all the routes in the application';

    /**
     * RouteCommand constructor.
     */
    public function __construct()
    {
        parent::__construct(self::COMMAND_NAME);
    }

    /**
     * Configure the command
     */
    protected function configure(): void
    {
        $this->setDescription(self::COMMAND_DESCRIPTION);
        $this->setHelp(self::COMMAND_HELP);
        $this->addArgument('route', null, 'Optional argument to show details for specific route', null);
    }

    /**
     * Execute the command
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $routes = Application::getRoutes();

        if (is_numeric($input->getArgument('route'))) {
            echo 'Details for route #' . $input->getArgument('route') . PHP_EOL;
            foreach ($routes as $id => $route) {
                if ($id == $input->getArgument('route')) {
                    var_dump($this->parseRoute($route)) . PHP_EOL;
                    return Command::SUCCESS;
                }
            }
            echo 'Route not found' . PHP_EOL;
            return Command::FAILURE;
        } else {
            echo 'List of routes' . PHP_EOL;

            foreach ($routes as $id => $route) {
                echo '#' . $id . ' - ' . $route . PHP_EOL;
            }

            return Command::SUCCESS;
        }
        return Command::FAILURE;
    }

    /**
     * Parse a route definition
     */
    protected function parseRoute(string $route): array
    {
        // Route example:
        // 'GET /about about@index'
        $route = explode(' ', $route);
        count($route) === 3 or die('Invalid route definition');
        $httpMethod = $route[0];
        $routePath = $route[1];
        $routeControllerAndMethod = explode('@', $route[2]);
        $controller = $routeControllerAndMethod[0];
        $controllerMethod = $routeControllerAndMethod[1];

        return [
            'httpMethod' => $httpMethod,
            'routePath' => $routePath,
            'controller' => [
                'baseName' => $controller,
                'fileName' => ucfirst($controller) . 'Controller.php',
                'className' => ucfirst($controller) . 'Controller',
            ],
            'controllerMethod' => $controllerMethod,
        ];
    }
}
