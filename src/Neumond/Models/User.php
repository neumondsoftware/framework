<?php

namespace Neumond\Models;

use Neumond\Core\Model;
use PDO;

/**
 * Class User
 * @package Neumond\Models
 */
class User extends Model {
    /**
     * @return array
     */
    public function getAllUsers() {
        $stmt = $this->pdo->query('SELECT * FROM users');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
