<?php

namespace Neumond\Core;

use PDO;

/**
 * Class Model
 * @package Neumond\Core
 */
class Model {
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * Model constructor.
     * @param array|null $credentials
     * @throws \Exception
     */
    public function __construct($credentials = null) {
        if ($credentials) {
            $this->pdo = new PDO('mysql:host=' .
            $credentials['host'] . ';dbname=' .
            $credentials['database'],
            $credentials['username'],
            $credentials['password']);
        } else {
            throw new \Exception('Database credentials not provided');
        }
    }
}
