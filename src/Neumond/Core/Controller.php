<?php

namespace Neumond\Core;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

use Neumond\Application;

/**
 * Class Controller
 * @package Neumond\Core
 */
class Controller {
    /**
     * @param string $view
     * @param array $data
     */
    protected function view($view, $data = []) {
        $loader = new FilesystemLoader(Application::getProjectRoot() . '/app/views');
        $twig = new Environment($loader);
        echo $twig->render($view . '.twig', $data);
    }
}
