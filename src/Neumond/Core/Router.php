<?php

namespace Neumond\Core;

/**
 * Class Router
 * @package Neumond\Core
 */
class Router {
    /**
     * @var array
     */
    private $routes = [];

    /**
     * Router constructor.
     * @param array $routes
     */
    public function __construct(array $routes) {
        foreach ($routes as $routeId => $routeDefinition) {
            $routeDefinition = explode(' ', $routeDefinition);
            $httpMethod = $routeDefinition[0];
            $routePath = $routeDefinition[1];
            $routeControllerAndMethod = explode('@', $routeDefinition[2]);
            $controller = $routeControllerAndMethod[0];
            $controllerMethod = $routeControllerAndMethod[1];
            $this->routes[] = [
                'path' => $routePath,
                'controller' => $controller,
                'function' => $controllerMethod,
                'method' => $httpMethod
            ];
        }
    }

    /**
     * @param string $path
     * @param callable $callback
     */
    public function get($path, $callback) {
        $this->routes[$path] = $callback;
    }

    /**
     * @param string $path
     * @param callable $callback
     */
    public function post($path, $callback) {
        $this->routes[$path] = $callback;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCurrentRoute() {
        $path = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];

        if ($path == '/') {
            $route = $this->routes[0];
            return [$route['controller'], $route['method']];
        }

        foreach ($this->routes as $route) {
            if ($route['path'] == $path && $route['method'] == $method) {
                return [$route['controller'], $route['function']];
            }
        }

        throw new \Exception('Route not found', 404);
    }

    /**
     * @throws \Exception
     */
    public function run() {
        $path = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];

        foreach ($this->routes as $route => $callback) {
            if ($route === $path) {
                if (is_callable($callback)) {
                    $callback();
                } else {
                    $controller = new $callback[0];
                    $method = $callback[1];
                    $controller->$method();
                }
            }
        }
    }
}
