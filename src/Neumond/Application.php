<?php

namespace Neumond;

use Neumond\Core\Router;

/**
 * Class Application
 * @package Neumond
 */
class Application {
    /**
     * @var string
     */
    protected $controller = 'welcome';

    /**
     * @var string
     */
    protected $method = 'index';

    /**
     * @var array
     */
    protected $params = [];

    /**
     * App constructor.
     * @param array $router
     * @throws \Exception
     */
    public function __construct($router) {
        $targetController = $this->dispatchRouter($router);

        if (is_array($targetController) && file_exists($this->getProjectRoot() . '/app/http/' . ucfirst($targetController[0]) . 'Controller.php')) {
            $this->controller = ucfirst($targetController[0]);
        } else {
            $this->controller = ucfirst($this->controller);
        }

        $controllerFile = $this->getProjectRoot() . '/app/http/' . $this->controller . 'Controller.php';
        if (file_exists($controllerFile)) {
            require_once $controllerFile;
            $controllerClass = "\\Neumond\\Controllers\\" . $this->controller . 'Controller';
            if (class_exists($controllerClass)) {
                $this->controller = new $controllerClass;
            } else {
                throw new \Exception("Controller class {$controllerClass} does not exist");
            }
        } else {
            throw new \Exception("Controller file {$controllerFile} does not exist");
        }

        if (isset($targetController[1])) {
            if (method_exists($this->controller, $targetController[1])) {
                $this->method = $targetController[1];
            }
        }

        $this->params = $targetController ? array_values($targetController) : [];

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    /**
     * @param array $router
     * @return mixed
     */
    private function dispatchRouter(array $router) {
        $router = new Router($router);
        return $router->getCurrentRoute();
    }

    /**
     * Get project root path
     * @return string
     */
    public static function getProjectRoot() {
        $libDir = __DIR__;
        $inVendor = strpos($libDir, 'vendor');
        if ($inVendor !== false) {
            return dirname($libDir, 5);
        }
        return dirname($libDir, 2);
    }

    /**
     * Get routes
     * @return mixed
     */
    public static function getRoutes() {
        $routeFilePath = self::getProjectRoot() . '/app/routes.php';
        file_exists($routeFilePath) or die('Routes file does not exist');
        require_once $routeFilePath;
        return $routes;
    }
}
