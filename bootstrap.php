<?php

if (!defined('AUTOLOAD_INCLUDED')) {
    define('AUTOLOAD_INCLUDED', true);

    require __DIR__ . '/vendor/autoload.php';
}
