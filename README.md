# Neumond Framework

Experimental PHP Framework

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

You can include this framework in your project like this:
```shell
composer require neumond/framework
```

## Usage

To use this framework is best to start with `neumond/skeleton` project, which is a starter kit to use this framework.

If you want to run just the framework, you can do it like this:
```shell
php blazar serve
```

You can access the framework at http://localhost:8000/

## Contributing

Feel free to create MR with your contributions.

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](./LICENSE)

This project is licensed under the terms of the [MIT license](./LICENSE).
