<?php

namespace Neumond\Controllers;

use Neumond\Core\Controller;

/**
 * Class WelcomeController
 * @package Neumond\Controllers
 */
class WelcomeController extends Controller
{
    /**
     * Main entry point for framework welcome page
     */
    public function index()
    {
        $this->view('welcome');
    }

    /**
     * About page
     */
    public function about()
    {
        $version = self::getFrameworkVersion();
        $this->view('about', ['version' => $version]);
    }

    public function getFrameworkVersion()
    {
        $composerJsonPath = __DIR__ . '/../../composer.json';
        $composerJsonContent = file_get_contents($composerJsonPath);
        $composerData = json_decode($composerJsonContent, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            die('Error parsing composer.json: ' . json_last_error_msg());
        }

        return $composerData['version'];
    }
}
